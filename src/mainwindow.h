#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QVBoxLayout>
#include <QWidget>
#include <base/hierarchylist.h>
#include <base/workspacewidget.h>

class MainWindow : public QWidget {
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private:
    QHBoxLayout* layout_;
};
#endif // MAINWINDOW_H
