#include "factorial.h"

Factorial::Factorial(QWidget* parent)
    : WorkSpaceWidget(parent)
{
    setTitle(tr("Факториал"));
    setDescription("Факториа́л — функция, определённая"
                   " на множестве неотрицательных целых чисел."
                   " Название происходит от лат. factorialis"
                   " — действующий, производящий, умножающий;"
                   " обозначается n!, произносится эн факториа́л."
                   " Факториал натурального числа n определяется"
                   " как произведение всех натуральных чисел"
                   " от 1 до n включительно");

    auto header_1 = new TextWidget(tr("Введите число n, расчёт произойдёт автоматически"), 12, true);
    header_1->setAlignment(Qt::AlignLeft);
    header_1->setFixedHeight(header_1->sizeHint().height());
    auto result_row = new QHBoxLayout;
    auto result_head_1 = new TextWidget(tr("Факториал от"), 10, true);
    result_head_1->setAlignment(Qt::AlignCenter);
    auto n_line = new QLineEdit;
    n_line->setPlaceholderText(QStringLiteral("n"));
    n_line->setAlignment(Qt::AlignCenter);
    n_line->setStyleSheet(QStringLiteral("background: rgb(245, 245, 245); border-radius: 10px;"));
    n_line->setFixedHeight(n_line->sizeHint().height() * 1.5);
    auto n_line_validator = new QIntValidator;
    n_line_validator->setBottom(0);
    n_line->setValidator(n_line_validator);
    auto result_head_2 = new TextWidget(tr(" равен "), 10, true);
    result_head_2->setAlignment(Qt::AlignCenter);
    auto result_line = new QLineEdit;
    result_line->setReadOnly(true);
    result_line->setAlignment(Qt::AlignCenter);
    result_line->setStyleSheet(QStringLiteral("background: rgb(245, 245, 245); border-radius: 10px;"));
    result_line->setFixedHeight(n_line->height());
    result_row->addWidget(result_head_1);
    result_row->addWidget(n_line);
    result_row->addWidget(result_head_2);
    result_row->addWidget(result_line);
    result_row->addStretch();
    layout_->addLayout(result_row);

    error_text_ = new QLabel;
    error_text_->setStyleSheet(QStringLiteral("background: white; color: red;"));
    error_text_->setAlignment(Qt::AlignCenter);
    QSizePolicy sp_retain = error_text_->sizePolicy();
    sp_retain.setRetainSizeWhenHidden(true);
    error_text_->setSizePolicy(sp_retain);

    layout_->addWidget(error_text_);
    layout_->addStretch(1);

    connect(n_line,
            &QLineEdit::textChanged,
            [n_line, result_line, this]() {
                if (n_line->hasAcceptableInput()){
                    auto result = Algorithms::computeFactorial(n_line->text());
                    if (result.err) {
                        switch (result.err) {
                        case Err::Overflow: {
                            error_text_->setText(tr("Переполнение результата"));
                            }
                        }
                        error_text_->show();
                    } else error_text_->hide();
                    result_line->setText(result.value);
                } else result_line->setText(QStringLiteral(""));

    });
}
