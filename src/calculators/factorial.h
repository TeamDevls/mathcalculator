#ifndef FACTORIAL_H
#define FACTORIAL_H

#include <base/workspacewidget.h>

class Factorial : public WorkSpaceWidget {
    Q_OBJECT
public:
    Factorial(QWidget* parent = nullptr);
};

#endif // FACTORIAL_H
