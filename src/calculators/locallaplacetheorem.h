#ifndef LOCALLAPLACETHEOREM_H
#define LOCALLAPLACETHEOREM_H

#include <base/workspacewidget.h>

class LocalLaplaceTheorem : public WorkSpaceWidget {
    Q_OBJECT
public:
    LocalLaplaceTheorem(QWidget* parent = nullptr);
private:
    QLineEdit *n_line_;
    QLineEdit *p_line_;
    QLineEdit *m_line_;
    QLineEdit *result_line_;
private slots:
    void compute();
};

#endif // LOCALLAPLACETHEOREM_H
