#ifndef DISCRETEEXPECTATION_H
#define DISCRETEEXPECTATION_H

#include <QTableWidget>
#include <base/workspacewidget.h>

//Математическое ожидание дискретной величины

class DiscreteExpectation : public WorkSpaceWidget {
    Q_OBJECT
public:
    DiscreteExpectation(QWidget* parent = nullptr);

private:
    QTableWidget* table_;
    TextWidget* r_result_1_;
    TextWidget* r_result_2_;
    TextWidget* r_result_3_;
private slots:
    void compute();
};

#endif // DISCRETEEXPECTATION_H
