#ifndef PLACEMENTSWIDGET_H
#define PLACEMENTSWIDGET_H

#include <base/workspacewidget.h>

class PlacementsWidget : public WorkSpaceWidget {
    Q_OBJECT
public:
    PlacementsWidget(QWidget* parent = nullptr);
private:
    QLineEdit *n_line_;
    QLineEdit *k_line_;
    QLineEdit *result_line_;
    QCheckBox *repeat_;
private slots:
    void compute();
};

#endif // PLACEMENTSWIDGET_H
