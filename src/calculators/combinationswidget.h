#ifndef COMBINATIONSWIDGET_H
#define COMBINATIONSWIDGET_H

#include <base/workspacewidget.h>

class CombinationsWidget : public WorkSpaceWidget {
    Q_OBJECT
public:
    explicit CombinationsWidget(QWidget* parent = nullptr);
private:
    QLineEdit *n_line_;
    QLineEdit *k_line_;
    QLineEdit *result_line_;
    QCheckBox *repeat_;
private slots:
    void compute();
};

#endif // COMBINATIONSWIDGET_H
