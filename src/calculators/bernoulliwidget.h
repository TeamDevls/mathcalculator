#ifndef BERNOULLIWIDGET_H
#define BERNOULLIWIDGET_H

#include <base/workspacewidget.h>

class BernoulliWidget : public WorkSpaceWidget
{
	Q_OBJECT
public:
    BernoulliWidget(QWidget *parent = nullptr);
private:
    QLineEdit *n_line_;
    QLineEdit *p_line_;
    QLineEdit *m_line_;
    QLineEdit *result_line_;
private slots:
    void compute();
};

#endif // BERNOULLIWIDGET_H
