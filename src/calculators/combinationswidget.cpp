#include "combinationswidget.h"

CombinationsWidget::CombinationsWidget(QWidget* parent)
    : WorkSpaceWidget(parent)
{
    setTitle(tr("Сочетания"));
    setDescription("Пусть имеется n различных объектов."
                   " Будем выбирать из них m объектов все возможными способами"
                   " (то есть меняется состав выбранных объектов, но порядок не важен)."
                   " Получившиеся комбинации называются сочетаниями"
                   " из n объектов по m, а их число равно"
                   " C(m, n)=n!/((n−m)!⋅m!)");

    auto result_row_1 = new QHBoxLayout;
    auto result_head_1 = new TextWidget(tr("Число сочетаний из"), 10, true);
    result_head_1->setAlignment(Qt::AlignCenter);
    n_line_ = new QLineEdit;
    n_line_->setPlaceholderText(tr("n"));
    n_line_->setAlignment(Qt::AlignCenter);
    n_line_->setStyleSheet(QStringLiteral("background: rgb(245, 245, 245); border-radius: 10px;"));
    n_line_->setFixedHeight(n_line_->sizeHint().height() * 1.5);
    auto n_line_validator = new QIntValidator;
    n_line_validator->setBottom(0);
    n_line_->setValidator(n_line_validator);
    auto result_head_2 = new TextWidget(tr("элементов по "), 10, true);
    result_head_2->setAlignment(Qt::AlignCenter);
    k_line_ = new QLineEdit;
    k_line_->setPlaceholderText(QStringLiteral("k"));
    k_line_->setAlignment(Qt::AlignCenter);
    k_line_->setStyleSheet(QStringLiteral("background: rgb(245, 245, 245); border-radius: 10px;"));
    k_line_->setFixedHeight(k_line_->sizeHint().height() * 1.5);
    auto k_line_validator = new QIntValidator;
    k_line_validator->setBottom(0);
    k_line_->setValidator(k_line_validator);

    result_row_1->addWidget(result_head_1);
    result_row_1->addWidget(n_line_);
    result_row_1->addWidget(result_head_2);
    result_row_1->addWidget(k_line_);
    result_row_1->addStretch();
    layout_->addLayout(result_row_1);

    auto result_row_2 = new QHBoxLayout;
    auto result_head_4 = new TextWidget(tr("Есть повторения"), 10, true);
    result_head_4->setFixedHeight(result_head_4->sizeHint().height() * 1.2);
    result_head_4->setAlignment(Qt::AlignCenter);
    repeat_ = new QCheckBox;
    result_row_2->addWidget(result_head_4);
    result_row_2->addWidget(repeat_);
    result_row_2->addStretch();
    layout_->addLayout(result_row_2);

    auto result_row_3 = new QHBoxLayout;
    auto result_head_3 = new TextWidget(tr("Результат"), 10, true);
    result_head_3->setAlignment(Qt::AlignCenter);
    result_line_ = new QLineEdit;
    result_line_->setReadOnly(true);
    result_line_->setAlignment(Qt::AlignCenter);
    result_line_->setStyleSheet(QStringLiteral("background: rgb(245, 245, 245); border-radius: 10px;"));
    result_line_->setFixedHeight(n_line_->height());
    result_row_3->addWidget(result_head_3);
    result_row_3->addWidget(result_line_);
    result_row_3->addStretch(0);
    layout_->addLayout(result_row_3);

    error_text_ = new QLabel;
    error_text_->setStyleSheet(QStringLiteral("background: white; color: red;"));
    error_text_->setAlignment(Qt::AlignCenter);
    QSizePolicy sp_retain = error_text_->sizePolicy();
    sp_retain.setRetainSizeWhenHidden(true);
    error_text_->setSizePolicy(sp_retain);

    layout_->addWidget(error_text_);
    layout_->addStretch(1);

    connect(n_line_, &QLineEdit::textChanged,
        this, &CombinationsWidget::compute);
    connect(k_line_, &QLineEdit::textChanged,
        this, &CombinationsWidget::compute);
    connect(repeat_, &QCheckBox::stateChanged,
        this, &CombinationsWidget::compute);
}

void CombinationsWidget::compute()
{
    if (n_line_->hasAcceptableInput() && k_line_->hasAcceptableInput()) {
        auto result = Algorithms::computeCombinations(n_line_->text(),
            k_line_->text(),
            repeat_->isChecked());
        if (result.err) {
            switch (result.err) {
            case Err::Overflow: {
                error_text_->setText(tr("Переполнение результата"));
                break;
            }
            case Err::nLessThanK: {
                error_text_->setText(tr("n должно быть больше, чем k!"));
            }
            }
            error_text_->show();
        } else
            error_text_->hide();
        result_line_->setText(result.value);
    } else
        result_line_->setText(QStringLiteral(""));
}
