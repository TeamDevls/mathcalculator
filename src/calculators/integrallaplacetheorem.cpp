#include "calculators/integrallaplacetheorem.h"

IntegralLaplaceTheorem::IntegralLaplaceTheorem(QWidget* parent)
    : WorkSpaceWidget(parent)
{
    setTitle(tr("Интегральная теорема Лапласа"));
    setDescription("Интегральная теорема Муавра-Лапласа"
                   " необходима в случаях,"
                   " когда задан диапазон возможного"
                   " количества появлений события");

    auto result_row_1 = new QHBoxLayout;
    auto result_head_1 = new TextWidget(tr("При"), 10, true);
    result_head_1->setAlignment(Qt::AlignCenter);
    n_line_ = new QLineEdit;
    n_line_->setPlaceholderText(QStringLiteral("n"));
    n_line_->setAlignment(Qt::AlignCenter);
    n_line_->setStyleSheet(QStringLiteral("background: rgb(245, 245, 245); border-radius: 10px;"));
    n_line_->setFixedHeight(n_line_->sizeHint().height() * 1.5);
    auto n_line_validator = new QIntValidator;
    n_line_validator->setBottom(0);
    n_line_->setValidator(n_line_validator);
    auto result_head_2 = new TextWidget(tr("независимых испытаний"), 10, true);
    result_head_2->setAlignment(Qt::AlignCenter);
    result_row_1->addWidget(result_head_1);
    result_row_1->addWidget(n_line_);
    result_row_1->addWidget(result_head_2);
    result_row_1->addStretch();
    layout_->addLayout(result_row_1);

    auto result_row_2 = new QHBoxLayout;
    auto result_head_3 = new TextWidget(tr("С вероятностью"), 10, true);
    result_head_3->setAlignment(Qt::AlignCenter);
    p_line_ = new QLineEdit;
    p_line_->setPlaceholderText(QStringLiteral("p"));
    p_line_->setAlignment(Qt::AlignCenter);
    p_line_->setStyleSheet(QStringLiteral("background: rgb(245, 245, 245); border-radius: 10px;"));
    p_line_->setFixedHeight(p_line_->sizeHint().height() * 1.5);
    auto p_line_validator = new QDoubleValidator;
    p_line_validator->setBottom(0.0);
    p_line_validator->setTop(1.0);
    p_line_validator->setLocale(QLocale::C);
    p_line_->setValidator(p_line_validator);
    auto result_head_4 = new TextWidget(tr("появления события в каждом испытании"), 10, true);
    result_head_4->setAlignment(Qt::AlignCenter);
    result_row_2->addWidget(result_head_3);
    result_row_2->addWidget(p_line_);
    result_row_2->addWidget(result_head_4);
    result_row_2->addStretch();
    layout_->addLayout(result_row_2);

    auto result_row_3 = new QHBoxLayout;
    auto result_head_5 = new TextWidget(tr("Вероятность, что событие наступит не менее"), 10, true);
    result_head_5->setAlignment(Qt::AlignLeft);
    m1_line_ = new QLineEdit;
    m1_line_->setPlaceholderText(QStringLiteral("m1"));
    m1_line_->setAlignment(Qt::AlignCenter);
    m1_line_->setStyleSheet(QStringLiteral("background: rgb(245, 245, 245); border-radius: 10px;"));
    m1_line_->setFixedHeight(m1_line_->sizeHint().height() * 1.5);
    auto m1_line_validator = new QIntValidator;
    m1_line_validator->setBottom(0);
    m1_line_->setValidator(m1_line_validator);
    result_row_3->addWidget(result_head_5);
    result_row_3->addWidget(m1_line_);
    result_row_3->addStretch();
    layout_->addLayout(result_row_3);

    auto result_row_4 = new QHBoxLayout;
    auto result_head_6 = new TextWidget(tr("но не более"), 10, true);
    result_head_6->setAlignment(Qt::AlignCenter);
    m2_line_ = new QLineEdit;
    m2_line_->setPlaceholderText(QStringLiteral("m2"));
    m2_line_->setAlignment(Qt::AlignCenter);
    m2_line_->setStyleSheet(QStringLiteral("background: rgb(245, 245, 245); border-radius: 10px;"));
    m2_line_->setFixedHeight(m2_line_->sizeHint().height() * 1.5);
    auto m2_line_validator = new QIntValidator;
    m2_line_validator->setBottom(0);
    m2_line_->setValidator(m2_line_validator);
    auto result_head_7 = new TextWidget(tr("раз"), 10, true);
    result_head_7->setAlignment(Qt::AlignCenter);
    result_row_4->addWidget(result_head_6);
    result_row_4->addWidget(m2_line_);
    result_row_4->addWidget(result_head_7);
    result_row_4->addStretch();
    layout_->addLayout(result_row_4);

    auto result_row_5 = new QHBoxLayout;
    auto result_head_8 = new TextWidget(tr("Будет равна"), 10, true);
    result_head_8->setAlignment(Qt::AlignCenter);
    result_line_ = new QLineEdit;
    result_line_->setReadOnly(true);
    result_line_->setAlignment(Qt::AlignCenter);
    result_line_->setStyleSheet(QStringLiteral("background: rgb(245, 245, 245); border-radius: 10px;"));
    result_line_->setFixedHeight(result_line_->sizeHint().height() * 1.5);
    result_row_5->addWidget(result_head_8);
    result_row_5->addWidget(result_line_);
    result_row_5->addStretch(0);
    layout_->addLayout(result_row_5);

    error_text_ = new QLabel;
    error_text_->setStyleSheet(QStringLiteral("background: white; color: red;"));
    error_text_->setAlignment(Qt::AlignCenter);
    QSizePolicy sp_retain = error_text_->sizePolicy();
    sp_retain.setRetainSizeWhenHidden(true);
    error_text_->setSizePolicy(sp_retain);
    layout_->addWidget(error_text_);

    auto compute_btn = new AnimatedButton(tr("Рассчитать"));
    compute_btn->setFixedWidth(compute_btn->sizeHint().width() * 1.5);
    compute_btn->setMinimumHeight(compute_btn->sizeHint().height());
    connect(compute_btn, &AnimatedButton::clicked,
        this, &IntegralLaplaceTheorem::compute);
    layout_->addWidget(compute_btn);

    layout_->addStretch(1);
}

void IntegralLaplaceTheorem::compute()
{
    if (n_line_->hasAcceptableInput() && p_line_->hasAcceptableInput()
        && m1_line_->hasAcceptableInput() && m2_line_->hasAcceptableInput()) {
        result_line_->setText(Algorithms::computeLaplaceIntegral(n_line_->text(), p_line_->text(), m1_line_->text(), m2_line_->text()));
    } else {
        error_text_->setText(tr("Недопустимый ввод"));
        error_text_->show();
    }
}
