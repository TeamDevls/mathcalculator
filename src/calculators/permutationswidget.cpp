#include "permutationswidget.h"

PermutationsWidget::PermutationsWidget(QWidget* parent)
    : WorkSpaceWidget(parent)
{
    setTitle(tr("Перестановки"));
    setDescription("Пусть имеется n различных объектов."
                   " Будем переставлять их всеми возможными способами"
                   "(число объектов остается неизменными, меняется только их порядок)."
                   " Получившиеся комбинации называются перестановками, а их число равно"
                   " P(n)=n!=1⋅2⋅3⋅...⋅(n−1)⋅n");
}
