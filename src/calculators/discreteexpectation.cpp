#include "discreteexpectation.h"

#include <QHeaderView>
#include <base/animatedbutton.h>
#include <base/scrollareawrapper.h>
#include <qtablewidget.h>

DiscreteExpectation::DiscreteExpectation(QWidget* parent)
    : WorkSpaceWidget(parent)
{
    setTitle(tr("Математическое ожидание дискретной величины"));
    setDescription("Дискретной случайной величиной"
                   " (англ. discrete random variable)"
                   " называется случайная величина, множество"
                   " значений которой не более чем счётно,"
                   " причём принятие ею каждого из значений"
                   " есть случайное событие с определённой вероятностью.");
    auto scroll_area = new ScrollAreaWrapper;
    auto header_1 = new TextWidget(tr("Заполните таблицу и нажмите \"Рассчитать\""), 12, true);
    header_1->setAlignment(Qt::AlignLeft);
    header_1->setFixedHeight(header_1->sizeHint().height());

    table_ = new QTableWidget(2, 5);
    table_->horizontalHeader()->hide();
    table_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    table_->setVerticalHeaderLabels({ "Xi", "Pi" });
    table_->setFixedHeight(table_->minimumSizeHint().height() + table_->horizontalScrollBar()->sizeHint().height());
    table_->setStyleSheet(QStringLiteral("* {background-color: white;}"
                                         "QScrollBar::handle:horizontal {background: rgb(224, 224, 224);}"
                                         "QScrollBar::sub-page:horizontal, QScrollBar::add-page:horizontal {background: rgb(240,240,240);}"
                                         "QScrollBar:left-arrow:horizontal, QScrollBar::right-arrow:horizontal {width: 0px; height: 0px;}"
                                         "QScrollBar::add-line:horizontal, QScrollBar::sub-line:horizontal {width: 0px; height: 0px;}"));

    auto table_controls_layout = new QHBoxLayout;
    auto btn_pls = new AnimatedButton(tr("Добавить колонку"));
    btn_pls->setFixedHeight(btn_pls->sizeHint().height());
    connect(btn_pls, &AnimatedButton::clicked, [this]() {
        table_->setColumnCount(table_->columnCount() + 1);
    });
    auto btn_minus = new AnimatedButton(tr("Удалить колонку"));
    btn_minus->setFixedHeight(btn_pls->sizeHint().height());
    connect(btn_minus, &AnimatedButton::clicked, [this]() {
        table_->removeColumn(table_->columnCount() - 1);
    });
    table_controls_layout->addWidget(btn_minus);
    table_controls_layout->addWidget(btn_pls);

    auto header_2 = new TextWidget(tr("Вычисления"), 12, true);
    header_2->setAlignment(Qt::AlignLeft);
    header_2->setFixedHeight(header_2->sizeHint().height());

    auto result_row_1 = new QHBoxLayout;
    auto r_header_1 = new TextWidget(tr("Математическое ожидание"), 10);
    r_header_1->setAlignment(Qt::AlignLeft);
    r_result_1_ = new TextWidget(tr("Не определено"), 10, true);
    r_result_1_->setAlignment(Qt::AlignCenter);
    r_result_1_->setFixedHeight(r_result_1_->sizeHint().height());
    //r_result_1_->setState(STATE::HOVERED);
    result_row_1->addWidget(r_header_1);
    result_row_1->addWidget(r_result_1_);

    auto result_row_2 = new QHBoxLayout;
    auto r_header_2 = new TextWidget(tr("Дисперсия"), 10);
    r_header_2->setAlignment(Qt::AlignLeft);
    r_result_2_ = new TextWidget(tr("Не определено"), 10, true);
    r_result_2_->setAlignment(Qt::AlignCenter);
    r_result_2_->setFixedHeight(r_result_2_->sizeHint().height());
    // r_result_2_->setState(STATE::HOVERED);
    result_row_2->addWidget(r_header_2);
    result_row_2->addWidget(r_result_2_);

    auto result_row_3 = new QHBoxLayout;
    auto r_header_3 = new TextWidget(tr("Среднее квадратическое отклонение"), 10);
    r_header_3->setAlignment(Qt::AlignLeft);
    r_result_3_ = new TextWidget(tr("Не определено"), 10, true);
    r_result_3_->setAlignment(Qt::AlignCenter);
    r_result_3_->setFixedHeight(r_result_3_->sizeHint().height());
    //r_result_3_->setState(STATE::HOVERED);
    result_row_3->addWidget(r_header_3);
    result_row_3->addWidget(r_result_3_);

    auto compute_btn = new AnimatedButton(tr("Рассчитать"));
    compute_btn->setMinimumWidth(compute_btn->sizeHint().width() * 1.5);
    compute_btn->setMinimumHeight(compute_btn->sizeHint().height());
    connect(compute_btn, &AnimatedButton::clicked,
        this, &DiscreteExpectation::compute);

    scroll_area->addWidget(header_1);
    scroll_area->addWidget(table_);
    scroll_area->addLayout(table_controls_layout);
    scroll_area->addWidget(header_2);
    scroll_area->addLayout(result_row_1);
    scroll_area->addLayout(result_row_2);
    scroll_area->addLayout(result_row_3);
    scroll_area->addWidget(compute_btn, 0, Qt::AlignCenter);

    layout_->addWidget(scroll_area);
    layout_->addStretch();
}

void DiscreteExpectation::compute()
{
    std::vector<DiscretePair> source;

    auto n = table_->columnCount();

    for (auto i = 0; i < n; i++) {
        if (table_->item(0, i) == nullptr || table_->item(0, i)->text() == QStringLiteral("")
            || table_->item(1, i) == nullptr || table_->item(1, i)->text() == QStringLiteral(""))
            source.push_back({ QString::number(0), QString::number(0) });
        else
            source.push_back({ table_->item(0, i)->text(), table_->item(1, i)->text() });
    }
    for (auto& [x, p] : source)
        qDebug() << x << p;

    auto computed_expectation = Algorithms::computeDiscreteExpectation(source);
    r_result_1_->setText(computed_expectation);
    auto computed_dispersion = Algorithms::computeDispersion(source, computed_expectation);
    r_result_2_->setText(computed_dispersion);
    r_result_3_->setText(Algorithms::computeStandardDeviation(computed_dispersion));
}
