#ifndef PERMUTATIONSWIDGET_H
#define PERMUTATIONSWIDGET_H

#include <base/workspacewidget.h>

class PermutationsWidget : public WorkSpaceWidget {
    Q_OBJECT
public:
    explicit PermutationsWidget(QWidget* parent = nullptr);

signals:
};

#endif // PERMUTATIONSWIDGET_H
