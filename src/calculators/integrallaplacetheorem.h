#ifndef INTEGRALLAPLACETHEOREM_H
#define INTEGRALLAPLACETHEOREM_H

#include <base/workspacewidget.h>

class IntegralLaplaceTheorem : public WorkSpaceWidget
{
	Q_OBJECT
public:
    IntegralLaplaceTheorem(QWidget *parent = 0);
private:
    QLineEdit *n_line_;
    QLineEdit *p_line_;
    QLineEdit *m1_line_;
    QLineEdit *m2_line_;
    QLineEdit *result_line_;
private slots:
    void compute();
};

#endif // INTEGRALLAPLACETHEOREM_H
