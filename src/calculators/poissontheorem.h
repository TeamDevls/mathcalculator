#ifndef POISSONTHEOREM_H
#define POISSONTHEOREM_H

#include <base/workspacewidget.h>

class PoissonTheorem : public WorkSpaceWidget
{
	Q_OBJECT
public:
    PoissonTheorem(QWidget* parent = nullptr);
private:
    QLineEdit *n_line_;
    QLineEdit *p_line_;
    QLineEdit *k_line_;
    QLineEdit *result_line_;
private slots:
    void compute();
};

#endif // POISSONTHEOREM_H
