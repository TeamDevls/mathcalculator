#include "algorithms.h"
#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>


QString Algorithms::computeDiscreteExpectation(const std::vector<DiscretePair>& source)
{
    double expectation = 0.0;
    for (auto &each : source) {
        expectation += (each.x.toDouble()  * each.p.toDouble());
    }
    return QString::number(expectation);
}

QString Algorithms::computeDispersion(const std::vector<DiscretePair>& source, const QString& expectation)
{
    double dispersion = 0.0;
    for (auto &each : source) {
        dispersion += (std::pow(each.x.toDouble(),2)  * each.p.toDouble());
    }
    dispersion -= std::pow(expectation.toDouble(), 2);
    return QString::number(dispersion);
}

QString Algorithms::computeStandardDeviation(const QString& dispersion)
{
    return QString::number(std::sqrt(dispersion.toDouble()));
}

/**
@brief Вычисляет факториал числа n
*/
Result<QString> Algorithms::computeFactorial(const QString &n)
{
    auto n_internal = n.toLongLong();
    auto result = Internal::computeFactorial(n_internal);
    return {result.err, QString::number(result.value)};
}

/**
@brief Вычисляет сочетания из n по k
@param repeat - нужно ли считать с повторениями
@return Результат в виде строки
*/
Result<QString> Algorithms::computeCombinations(const QString &n, const QString &k, bool repeat)
{
    auto n_internal = n.toLongLong();
    auto k_internal = k.toLongLong();
    auto result = Internal::computeCombinations(n_internal, k_internal, repeat);
    return {result.err, QString::number(result.value)};
}

/**
@brief Вычисляет размещения из n по k
@param repeat - нужно ли считать с повторениями
@return Результат в виде строки
*/
Result<QString> Algorithms::computePlacements(const QString &n, const QString &k, bool repeat)
{
    auto n_internal = n.toLongLong();
    auto k_internal = k.toLongLong();
    auto result = Internal::computePlacements(n_internal, k_internal, repeat);

    return {result.err, QString::number(result.value)};
}

/**
@brief Вычисляет перестановки от n
@return Результат в виде строки
*/
Result<QString> Algorithms::computePermutations(const QString &n)
{
    return computeFactorial(n);
}

/**
@brief Вычисляет перестановки от n с повторениями со списком k аргументов
@details Перегруженная функция
@param n - число n, k - список чисел k
*/
Result<QString> Algorithms::computePermutations(const QString &n, QStringList &k)
{
    auto n_internal = n.toLongLong();
    QVector<long long> k_internal;
    long long k_max_ix = 0;
    long long k_max = 0;
    auto k_size = k.size();
    for (long long i = 0; i < k_size; i++) {
        auto next_k = k[i].toLongLong();
        if (next_k > n_internal)
            return {Err::nLessThanK, "0"};
        else if (next_k < 0)
            return {Err::NegativeNumber, "0"};
        else {
            if (next_k > k_max) {
                k_max_ix = i;
                k_max = next_k;
            }
            k_internal.push_back(next_k);
        }
    }

    k_internal.erase(k_internal.cbegin() + k_max_ix);

    auto numerator = Internal::multiplyAllInRange(n_internal - k_max + 1, n_internal);
    if (numerator.err != Err::NoError)
        return {numerator.err, "0"};

    auto result = numerator.value;
    for (auto &each : k_internal) {
        auto factorial = Internal::computeFactorial(each);
        if (factorial.err != Err::NoError)
            return {factorial.err, "0"};
        else
            result /= factorial.value;
    }
    return {Err::NoError, QString::number(result)};
}

/**
@brief Возводит n в степень k,
@return Результат в виде строки
*/
Result<QString> Algorithms::computePow(const QString &n, const QString &k)
{
    auto n_as_digit = n.toDouble();
    auto k_as_digit = k.toDouble();
    auto result = Internal::computePow(n_as_digit, k_as_digit);
    return {result.err, QString::number(result.value)};
}

/**
 * @brief Локальная теорема Лапласа
 * @param n - кол-во испытаний
 * @param p - вероятность успеха каждого испытания
 * @param m - кол-во успешных испытаний
 */
QString Algorithms::computeLaplaceLocal(const QString &n, const QString &p, const QString &m)
{
    auto n_d = n.toDouble();
    auto p_d = p.toDouble();
    auto m_d = m.toDouble();

    auto result = Internal::computeLaplaceLocal(n_d, p_d, m_d);
    return QString::number(result.value);
}

/**
    @brief Интегральная теорема Лапласа
    @param n - кол-во испытаний
    @param p - вероятность успеха каждого испытания
    @param m1 - 'не менее' раз
    @param m2 - 'не более' раз
*/
QString Algorithms::computeLaplaceIntegral(const QString &n, const QString &p, const QString &m1, const QString &m2) {
    auto n_d = n.toDouble();
    auto p_d = p.toDouble();
    auto m1_d = m1.toDouble();
    auto m2_d = m2.toDouble();

    auto result = Internal::computeLaplaceIntegral(n_d, p_d, m1_d, m2_d);
    return QString::number(result.value);
}

QVariantMap Algorithms::getMapFromJsonFile(const QString &filePath)
{
    QFile jsonFile(filePath);

    if(!jsonFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file");
    }

    QByteArray saveData = jsonFile.readAll();
    QJsonDocument jsonDocument(QJsonDocument::fromJson(saveData));
    QJsonObject jsonObject = jsonDocument.object();
    QVariantMap variantMap = jsonObject.toVariantMap();
    QMap<QString, QVariant>::const_iterator i = variantMap.constBegin();
    while (i != variantMap.constEnd()) {
        //qDebug() << i.key().toDouble() << ": " << i.value().toDouble() << Qt::endl;
        ++i;
    }
    return variantMap;
}

/**
 * @brief Формула Пуассона
 * @param n - кол-во испытаний
 * @param p - вероятность события
 * @param k - 'наступит раз'
 * @return
 */
Result<QString> Algorithms::computePoisson(const QString &n, const QString &p, const QString &k)
{
    auto n_d = n.toDouble();
    auto p_d = p.toDouble();
    auto k_d = k.toDouble();

    auto result = Internal::computePoisson(n_d, k_d, p_d);
    return {result.err, QString::number(result.value)};
}

/**
@brief Возводит n в степень k,
@return Результат в виде числа с плавающей точкой
*/
Result<double> Algorithms::Internal::computePow(double n, double k)
{
    auto result = std::pow(n, k);
    if (std::isinf(result))
        return {Err::Overflow, result};
    else
        return { Err::NoError, result };
}

/**
@brief Факториал числа n
@return Результат в виде числа с плавающей точкой
*/
Result<double> Algorithms::Internal::computeFactorial(long long n)
{
    if (n < 0)
        return {Err::NegativeNumber, 0.0};
    return Internal::multiplyAllInRange(1, n);
}

/**
@brief Перемножает все числа между собой,
@return Результат в виде числа с плавающей точкой
*/
Result<double> Algorithms::Internal::multiplyAllInRange(long long a, long long b)
{
    double result = 1.0;
    for (long long i = a; i <= b; i++) {
        result *= i;
        if (std::isinf(result))
            return {Err::Overflow, result};
    }
    return { Err::NoError , result };
}

/**
@brief Вычисляет сочетания из n по k
@param repeat - нужно ли считать с повторениями
@return Результат в виде числа с плавающей точкой
*/
Result<double> Algorithms::Internal::computeCombinations(long long n, long long k, bool repeat)
{
    if (n < k)
        return {Err::nLessThanK, 0.0};

    if (repeat)
        return Internal::computeCombinations(k + n - 1, k);
    else {
        auto numerator = Internal::multiplyAllInRange(n - k + 1, n);
        if (numerator.err != Err::NoError)
            return {numerator.err, 0.0};
        auto denominator = Internal::computeFactorial(k);
        if (denominator.err != Err::NoError)
            return {denominator.err, 0.0};

        auto result = numerator.value / denominator.value;
        return {Err::NoError, result};
    }
}

/**
@brief Вычисляет размещения из n по k
@param repeat - нужно ли считать с повторениями
@return Результат в виде числа с плавающей точкой
*/
Result<double> Algorithms::Internal::computePlacements(long long n, long long k, bool repeat)
{
    if (n < k)
        return {Err::nLessThanK, 0.0};

    if (repeat)
        return Internal::computePow(n, k);
    else
        return Internal::multiplyAllInRange(n - k + 1, n);
}

Result<double> Algorithms::Internal::computePermutations(long long n)
{
    return Internal::computeFactorial(n);
}

std::vector<double> Algorithms::Internal::generatingProbabilityFunction(std::vector<double> args)
{
    int n = args.size();
    auto arr = std::vector<double>(n + 1, 0);

    double p = 0, q = 0;
    bool t = true;
    for (int j = 0; j < n; j++)
    {
        p = args[j];
        q = 1 - p;
        if (t) {
            arr[0] = q;
            arr[1] = p;
            t = false;
        }
        else {
            for (int i = n; i != -1; i--)
            {
                if (i != 0)
                {
                    arr[i] = arr[i] * q + arr[i - 1] * p;
                }
                else
                {
                    arr[i] = arr[i] * q;
                }
            }
        }
    }
    return arr;
}

Result<double> Algorithms::Internal::computeBernoulli(long long n, long long m, double p)
{
    auto q = 1.0 - p;
    auto factor_1 = Internal::computeCombinations(n, m);
    if (factor_1.err)
        return {factor_1.err, 0.0};
    auto factor_2 = Internal::computePow(p, m);
    if (factor_2.err)
        return {factor_2.err, 0.0};
    auto factor_3 = Internal::computePow(q, n - m);
    if (factor_3.err)
        return {factor_3.err, 0.0};

    return {Err::NoError, factor_1.value * factor_2.value * factor_3.value};
}

Result<double> Algorithms::Internal::computePoisson(long long n, long long k, double p)
{
    auto lambda = n * p;
    if (lambda > 10)
        return {Err::PoissonLambdaTooBig, 0.0};
    auto k_factorial = Internal::computeFactorial(k);
    if (k_factorial.err == Err::NoError)
        return {Err::NoError, (pow(lambda, k) / k_factorial.value) * exp(-lambda)};
    else
        return {k_factorial.err, 0.0};
}

Result<double> Algorithms::Internal::computeLaplaceLocal(long long n, double p, long long m)
{
    double q = 1 - p;
    double x = ((m - p * n)/(sqrt(n * p * q)));
    double f;

    //При x > 4 функция крайне мала (-> 0 )

    if (x > 4) f = 0.0;
    else {
        f = 1 /(sqrt(2 * PI)) * exp(-pow(x, 2) / 2);
    }
    double result = 1 / sqrt(n * p * (1 - p));
    result = result * f;

    return {Err::NoError, result};
}

Result<double> Algorithms::Internal::computeLaplaceIntegral(long long n, double p, long long m1, long long m2)
{
    double q = 1 - p;
    double lambda = n * p;
    double denominator = sqrt(lambda * q);

    double x1 = (m1 - lambda) / denominator;
    double x2 = (m2 - lambda) / denominator;

    double f1;
    double f2;

    //При x > 5 функция равна 0.5
    if (auto module = abs(x1); module > 5.0)
        f1 = 0.5;
    else
        f1 = 0.5 * erf(module /sqrt(2));
    f1 = x1 > 0.0 ? f1 : -f1;

    if (auto module = abs(x2); module > 5.0)
        f2 = 0.5;
    else
        f2 = 0.5 * erf(module /sqrt(2));
    f2 = x2 > 0.0 ? f2 : -f2;

    return {Err::NoError, f2 - f1};
}

Result<QString> Algorithms::computeBernoulli(const QString &n, const QString &p, const QString &m)
{
    auto n_d = n.toLongLong();
    auto p_d = p.toDouble();
    auto m_d = m.toLongLong();

    auto result = Internal::computeBernoulli(n_d, m_d, p_d);
    return {result.err, QString::number(result.value)};
}
