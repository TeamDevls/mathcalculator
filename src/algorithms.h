#ifndef ALGORITHMS_H
#define ALGORITHMS_H
#include <QString>
#include <QtMath>

#define PI 3.141592653589793116

/**
 * @brief Обозначения ошибок
 */
enum Err:uchar {
    NoError = 0, ///< OK
    Overflow = 1, ///< Переполнение
    nLessThanK = 2, ///< N меньше чем K
    NullDenominator = 3, ///< Деление на н
    NegativeNumber = 4, ///< Отрицательное число
    PoissonLambdaTooBig = 5, ///< произведение n * p > 10
    Unknown = 126 ///< Неизвестно
};

/**
 * @brief Контейнер для хранения результата
 */
template<typename T>
struct Result {
    uchar err; ///< Код ошибки
    T value; ///< Значение результата
};


/**
 * @brief Структура, описывающая каждую пару дискретной величины

 */
struct DiscretePair {
    QString x; ///< значение дискретной величины
    QString p; ///< ее вероятность

};

namespace Algorithms {

/**
 * @brief Вычисляет математическое ожидание
 * @param source - массив DiscretePair
 * @return Результат в виде строки
 */
QString computeDiscreteExpectation(const std::vector<DiscretePair>& source);
/**
 * @brief computeDispersion
 * @param source - массив DiscretePair
 * @param expectation - математическое ожидание
 * @return Результат в виде строки
 */
QString computeDispersion(const std::vector<DiscretePair>& source, const QString& expectation);
/**
 * @brief Вычисляет среднеквадратичное отклонение
 * @param dispersion - дисперсия случайной величины
 * @return Результат в виде строки
 */
QString computeStandardDeviation(const QString& dispersion);
/**
 * @brief Вычисляет факториал числа n
*/
Result<QString> computeFactorial(const QString& n);
/**
 * @brief Вычисляет сочетания из n по k
 * @param repeat - нужно ли считать с повторениями
 * @return Результат в виде строки
*/
Result<QString> computeCombinations(const QString& n, const QString &k, bool repeat = false);
/**
 * @brief Вычисляет размещения из n по k
 * @param repeat - нужно ли считать с повторениями
 * @return Результат в виде строки
*/
Result<QString> computePlacements(const QString& n, const QString &k, bool repeat = false);
/**
 * @brief Возводит n в степень k,
 * @return Результат в виде строки
*/
Result<QString> computePow(const QString &n, const QString &k);
/**
 * @brief Вычисляет перестановки от n
 * @param n - число элементов всего
 * @return Результат в виде строки
*/
Result<QString> computePermutations(const QString &n);
/**
 * @brief Вычисляет перестановки от n с повторениями со списком k аргументов
 * @details Перегруженная функция
 * @param n - число элементов всего
 * @param k - число выбранных элементов их общего пула
 * @return Результат в виде строки
*/
Result<QString> computePermutations(const QString &n, QStringList &k);
/**
 * @brief Локальная теорема Лапласа
 * @param n - кол-во испытаний
 * @param p - вероятность успеха каждого испытания
 * @param m - кол-во успешных испытаний
 * @return Результат в виде строки
 */
QString computeLaplaceLocal(const QString &n, const QString &p, const QString &m);
/**
 * @brief Интегральная теорема Лапласа
 * @param n - кол-во испытаний
 * @param p - вероятность успеха каждого испытания
 * @param m1 - 'не менее' раз
 * @param m2 - 'не более' раз
 * @return Результат в виде строки
*/
QString computeLaplaceIntegral(const QString &n,
                               const QString &p,
                               const QString &m1,
                               const QString &m2);
QVariantMap getMapFromJsonFile(const QString &filePath);
/**
 * @brief Формула Пуассона
 * @param n - кол-во испытаний
 * @param p - вероятность события
 * @param k - 'произойдет раз'
 * @return Результат в виде строки
 */
Result<QString> computePoisson(const QString &n, const QString &p, const QString &k);
/**
 * @brief computeBernoulli
 * @param n - кол-во испытаний
 * @param p - вероятность события
 * @param m - 'произойдет раз'
 * @return Результат в виде строки
 */
Result<QString> computeBernoulli(const QString &n, const QString &p, const QString &m);

    namespace Internal {
    /**
     * @return
     */
    Result<double> multiplyAllInRange(long long a, long long b);
    /**
     * @return
     */
    Result<double> computePow(double n, double k);
    /**
     * @return
     */
    Result<double> computeFactorial(long long n);
    /**
     * @return
     */
    Result<double> computeCombinations(long long n, long long k, bool repeat = false);
    /**
     * @return
     */
    Result<double> computePlacements(long long n, long long k, bool repeat = false);
    /**
     * @return
     */
    Result<double> computePermutations(long long n);
    /**
     * @return
     */
    std::vector<double> generatingProbabilityFunction(std::vector<double> args);
    /**
     * @return
     */
    Result<double> computeBernoulli(long long n, long long m, double p);
    /**
     * @return
     */
    Result<double> computePoisson(long long n, long long k, double p);
    /**
     * @return
     */
    Result<double> computeLaplaceLocal(long long n, double p, long long m);
    /**
     * @return
     */
    Result<double> computeLaplaceIntegral(long long n, double p, long long m1, long long m2);
    }
};

#endif // ALGORITHMS_H
