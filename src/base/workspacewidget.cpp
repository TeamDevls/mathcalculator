#include "base/workspacewidget.h"

WorkSpaceWidget::WorkSpaceWidget(QWidget* parent)
    : QWidget(parent)
{
    title_ = new TextWidget(QStringLiteral(""), 14, true);
    title_->setAlignment(Qt::AlignLeft);
    description_ = new QPlainTextEdit;
    description_->setReadOnly(true);
    description_->horizontalScrollBar()->setFixedSize(0, 0);
    description_->setVerticalScrollBarPolicy(Qt ::ScrollBarAlwaysOff);

    layout_ = new QVBoxLayout();
    setLayout(layout_);
    layout_->addWidget(title_, 0);
    layout_->addWidget(description_);
}

void WorkSpaceWidget::paintEvent(QPaintEvent* event)
{
    Q_UNUSED(event);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setBrush(QBrush(AppStyle::backgroundColor(ROLE::SECONDARY, STATE::NORMAL)));
    painter.setPen(Qt::NoPen);
    painter.drawRect(rect());
}

void WorkSpaceWidget::setDescription(const QString& text)
{
    description_->setPlainText(text);
    description_->adjustSize();
    description_->setFixedHeight(description_->minimumSizeHint().height());
}

void WorkSpaceWidget::setTitle(const QString& text)
{
    title_->setText(text);
    title_->setFixedHeight(title_->sizeHint().height());
}
