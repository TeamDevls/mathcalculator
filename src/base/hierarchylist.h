#ifndef HIERARCHYLIST_H
#define HIERARCHYLIST_H

#include <QLineEdit>
#include <QWidget>
#include <animatedbutton.h>
#include <calculators/bernoulliwidget.h>
#include <calculators/combinationswidget.h>
#include <calculators/discreteexpectation.h>
#include <calculators/factorial.h>
#include <calculators/integrallaplacetheorem.h>
#include <calculators/locallaplacetheorem.h>
#include <calculators/permutationswidget.h>
#include <calculators/placementswidget.h>
#include <calculators/poissontheorem.h>
#include <hierarchytreewidget.h>
#include <scrollareawrapper.h>

class HierarchyList : public QWidget {
    Q_OBJECT
signals:
    void newWorkspaceCreated(WorkSpaceWidget* widget);

public:
    explicit HierarchyList(QWidget* parent = nullptr);
    template <class T>
    AnimatedButton* createButtonOpens(const QString& element_name)
    {
        auto button = new AnimatedButton(element_name, ROLE::SECONDARY, 0);
        button->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        connect(button,
            &AnimatedButton::clicked,
            this,
            [=]() {
                emit newWorkspaceCreated(new T);
            });
        return button;
    }

protected:
    virtual void paintEvent(QPaintEvent* event);

private:
    QLineEdit* search_line_;
};

#endif // HIERARCHYLIST_H
