#include "base/hierarchylist.h"
#include <QLabel>

HierarchyList::HierarchyList(QWidget* parent)
    : QWidget(parent)
{
    auto layout = new QVBoxLayout();
    layout->setContentsMargins(4, 4, 4, 0);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    setLayout(layout);

    search_line_ = new QLineEdit();
    search_line_->setPlaceholderText(tr("Калькулятор или категория"));
    search_line_->setAlignment(Qt::AlignCenter);
    search_line_->setMinimumHeight(search_line_->sizeHint().height() * 1.1);
    setStyleSheet(QString(tr("QLineEdit {background-color: %1;}"))
                      .arg(AppStyle::backgroundColor(ROLE::SECONDARY, STATE::HOVERED).name()));

    auto scroll_area = new ScrollAreaWrapper();
    scroll_area->setSpacings(0, 0, 0, 0, 0);

    auto combinatorics_category = new HierarchyTreeWidget(tr("Комбинаторика"));
    combinatorics_category->addElement(createButtonOpens<CombinationsWidget>(tr("Сочетания")));
    combinatorics_category->addElement(createButtonOpens<PermutationsWidget>(tr("Перестановки")));
    combinatorics_category->addElement(createButtonOpens<PlacementsWidget>(tr("Размещения")));

    auto repeat_actions_category = new HierarchyTreeWidget(tr("Повторные независимые \nиспытания"));
    repeat_actions_category->addElement(createButtonOpens<LocalLaplaceTheorem>(tr("Локальная теорема Лапласа")));
    repeat_actions_category->addElement(createButtonOpens<IntegralLaplaceTheorem>(tr("Интегральная теорема Лапласа")));
    repeat_actions_category->addElement(createButtonOpens<PoissonTheorem>(tr("Формула Пуассона")));
    repeat_actions_category->addElement(createButtonOpens<BernoulliWidget>(tr("Формула Бернулли")));

    auto other_category = new HierarchyTreeWidget(tr("Другое"));
    other_category->addElement(createButtonOpens<Factorial>(tr("Вычисление факториала")));

    scroll_area->addWidget(combinatorics_category);
    scroll_area->addWidget(repeat_actions_category);
    scroll_area->addWidget(createButtonOpens<DiscreteExpectation>(tr("Математическое ожидание\nдискретной величины")));
    scroll_area->addWidget(other_category);
    scroll_area->addStretch(1);
    /*
    auto space = new QLabel;
    space->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    space->setStyleSheet("background-color: white;");
    scroll_area->addWidget(space);*/

    layout->addWidget(search_line_);
    layout->addWidget(scroll_area);
}

void HierarchyList::paintEvent(QPaintEvent* event)
{
    Q_UNUSED(event);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setBrush(QBrush(AppStyle::backgroundColor(ROLE::SECONDARY, STATE::NORMAL)));
    painter.setPen(Qt::NoPen);
    painter.drawRect(rect());
}
