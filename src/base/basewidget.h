#ifndef BASEWIDGET_H
#define BASEWIDGET_H

#include <QPainter>
#include <QWidget>
#include <base/appstyle.h>

class BaseWidget : public QWidget {
    Q_OBJECT
public:
    BaseWidget(QWidget* parent = nullptr);
    void setRole(ROLE role)
    {
        role_ = role;
        update();
    }
    ROLE role() { return role_; }
    void setRadius(int r)
    {
        r_ = r;
        update();
    }
    int radius() { return r_; }
    STATE state() { return state_; }
    void setState(STATE state)
    {
        state_ = state;
        update();
    }

protected:
    int r_;
    ROLE role_;
    STATE state_;
    virtual void paintEvent(QPaintEvent* event) override;
};

#endif // BASEWIDGET_H
