#include "base/scrollareawrapper.h"
#include <QCoreApplication>
#include <QDebug>
#include <QPainter>
#include <QWheelEvent>
#include <base/appstyle.h>

ScrollAreaWrapper::ScrollAreaWrapper(QWidget* parent)
    : QScrollArea(parent)
{
    //setFixedWidth(500);
    scroll_vertical_ = true;
    //setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    setAlignment(Qt::Alignment(Qt::AlignHCenter | Qt::AlignTop));
    horizontalScrollBar()->setFixedSize(0, 0);
    verticalScrollBar()->setFixedSize(0, 0);

    setVerticalScrollBarPolicy(Qt ::ScrollBarAlwaysOff);
    setWidgetResizable(true);
    scroll_frame = new BaseWidget(this);
    scroll_frame->setRole(ROLE::SECONDARY);
    scroll_layout = new QVBoxLayout;
    scroll_frame->setLayout(scroll_layout);
    setWidget(scroll_frame);
}

void ScrollAreaWrapper::clear()
{
    while (QLayoutItem* item = scroll_layout->takeAt(0)) {
        item->widget()->deleteLater();
        delete item;
    }
}

void ScrollAreaWrapper::wheelEvent(QWheelEvent* e)
{
    if (!scroll_vertical_)
        QCoreApplication::sendEvent(horizontalScrollBar(), e);
    else
        QCoreApplication::sendEvent(verticalScrollBar(), e);
}

void ScrollAreaWrapper::addWidget(QWidget* widget, int stretch, Qt::Alignment alignment)
{
    scroll_layout->addWidget(widget, stretch, alignment);
}

void ScrollAreaWrapper::setSpacings(int l, int t, int r, int b, int between)
{
    scroll_layout->setContentsMargins(l, t, r, b);
    scroll_layout->setSpacing(between);
}
