#ifndef HIERARCHYTREEWIDGET_H
#define HIERARCHYTREEWIDGET_H

#ifndef EXPANDINGWEATHERWIDGET_H
#define EXPANDINGWEATHERWIDGET_H

#include <QPainter>
#include <QPropertyAnimation>
#include <QStyleOption>
#include <QVBoxLayout>
#include <QWidget>
#include <base/animatedbutton.h>
#include <base/appstyle.h>
#include <base/scrollareawrapper.h>

class HierarchyTreeWidget : public QWidget {
    Q_OBJECT
    Q_PROPERTY(int hidden_height READ getHeight WRITE setHeight) ///< hidden_frame height macro
public:
    HierarchyTreeWidget(const QString& category_name, QWidget* parent = nullptr);
    ~HierarchyTreeWidget();
    void addElement(QWidget* widget, int stretch = 0, Qt::Alignment alignment = Qt::Alignment());

private:
    ScrollAreaWrapper* scroll_area;
    QPropertyAnimation* animation; ///< Expand/collapse hidden_frame animation object
    int getHeight();
    void setHeight(int value);

protected:
    virtual void paintEvent(QPaintEvent* event);
};

#endif // EXPANDINGWEATHERWIDGET_H

#endif // HIERARCHYTREEWIDGET_H
