#include "base/hierarchytreewidget.h"

HierarchyTreeWidget::HierarchyTreeWidget(const QString &category_name, QWidget *parent)
    : QWidget(parent)
{
    auto main_layout = new QVBoxLayout;
    main_layout->setSpacing(0);
    setLayout(main_layout);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    main_layout->setContentsMargins(0, 0, 0, 0);

    auto header = new AnimatedButton(category_name, ROLE::SECONDARY, 0);
    header->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    header->setFontParams(10, true);
    connect(header,
        &AnimatedButton::clicked,
        this,
        [=]() {
            if (auto height = scroll_area->height(); height == 0) {

                animation->setStartValue(height);
                animation->setEndValue(scroll_area->sizeHint().height());
                animation->start();
                header->setRole(ROLE::PRIMARY);
            } else {
                scroll_area->hide();
                animation->setStartValue(height);
                animation->setEndValue(0);
                animation->start();
                header->setRole(ROLE::SECONDARY);
            }
        });

    scroll_area = new ScrollAreaWrapper();
    scroll_area->setSpacings(0, 0, 0, 0, 0);
    QSizePolicy sp_retain = scroll_area->sizePolicy();
    sp_retain.setRetainSizeWhenHidden(true);
    scroll_area->setSizePolicy(sp_retain);
    scroll_area->setFixedHeight(0);

    main_layout->addWidget(header);
    main_layout->addWidget(scroll_area);

    animation = new QPropertyAnimation(this, "hidden_height");
    animation->setDuration(200);
    connect(animation,
        &QPropertyAnimation::finished,
        this,
        [=]() {
            if (scroll_area->height())
                scroll_area->show();
        });
}

int HierarchyTreeWidget::getHeight()
{
    return scroll_area->height();
}

void HierarchyTreeWidget::setHeight(int value)
{
    scroll_area->setFixedHeight(value);
    update();
}

void HierarchyTreeWidget::paintEvent(QPaintEvent* event)
{
    Q_UNUSED(event);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setBrush(QBrush(AppStyle::backgroundColor(ROLE::SECONDARY, STATE::NORMAL)));
    painter.setPen(Qt::NoPen);
    painter.drawRect(rect());
}

HierarchyTreeWidget::~HierarchyTreeWidget()
{
}

void HierarchyTreeWidget::addElement(QWidget* widget, int stretch, Qt::Alignment alignment)
{
    scroll_area->addWidget(widget, stretch, alignment);
}
