#ifndef SCROLLAREAWRAPPER_H
#define SCROLLAREAWRAPPER_H

#include <QScrollArea>
#include <QScrollBar>
#include <QVBoxLayout>
#include <base/basewidget.h>

/**
@brief Small wrapper over QScrollArea
@details Here nothing to say. I added it to make
the logic of the code more transparent
*/

class ScrollAreaWrapper : public QScrollArea {
    Q_OBJECT
    BaseWidget* scroll_frame;
    QVBoxLayout* scroll_layout;
    bool scroll_vertical_;

public:
    ScrollAreaWrapper(QWidget* parent = nullptr);
    void addWidget(QWidget* widget, int stretch = 0, Qt::Alignment alignment = Qt::Alignment());
    void addStretch(int stretch = 0) { scroll_layout->addStretch(stretch); }
    void addLayout(QLayout* layout, int stretch = 0) { scroll_layout->addLayout(layout, stretch); }
    void setSpacings(int left, int top, int right, int bottom, int between);
    void clear();
    void setScrollType(bool scroll_vertical) { scroll_vertical_ = scroll_vertical; }

protected:
    virtual void wheelEvent(QWheelEvent* e) override;
};

#endif // SCROLLAREAWRAPPER_H
