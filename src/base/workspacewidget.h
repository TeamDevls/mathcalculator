#ifndef WORKSPACEWIDGET_H
#define WORKSPACEWIDGET_H

#include <QDoubleValidator>
#include <QGridLayout>
#include <QLabel>
#include <QCheckBox>
#include <QIntValidator>
#include <QLineEdit>
#include <QPainter>
#include <QPlainTextEdit>
#include <QScrollBar>
#include <QWidget>
#include <algorithms.h>
#include <base/appstyle.h>
#include <base/textwidget.h>
#include <base/animatedbutton.h>

class WorkSpaceWidget : public QWidget {
    Q_OBJECT
public:
    explicit WorkSpaceWidget(QWidget* parent = nullptr);

protected:
    virtual void paintEvent(QPaintEvent* event) override;
    TextWidget* title_;
    QVBoxLayout* layout_;
    QLabel* error_text_;
    void setDescription(const QString& text);
    void setTitle(const QString& text);

private:
    QPlainTextEdit* description_;
signals:
};

#endif // WORKSPACEWIDGET_H
