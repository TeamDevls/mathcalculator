#include "mainwindow.h"
#include <QLabel>
#include <QTableWidget>
MainWindow::MainWindow(QWidget* parent)
    : QWidget(parent)
{
    setStyleSheet(QStringLiteral("* {border: 0px;}"));
    layout_ = new QHBoxLayout();
    layout_->setContentsMargins(0, 0, 0, 0);
    layout_->setSpacing(4);
    setLayout(layout_);
    auto calc_list = new HierarchyList();
    connect(
        calc_list,
        &HierarchyList::newWorkspaceCreated,
        [this](WorkSpaceWidget* widget) {
            if (auto item = layout_->takeAt(1); item) {
                item->widget()->deleteLater();
            }
            layout_->addWidget(widget, 3);
        });
    layout_->addWidget(calc_list, 1);

    auto placeholder = new BaseWidget;
    placeholder->setRole(ROLE::SECONDARY);
    layout_->addWidget(placeholder, 3);
}

MainWindow::~MainWindow()
{
}
