var searchData=
[
  ['r_5f_363',['r_',['../class_base_widget.html#aee67de77e349b3da28f46f5ddc2e0aab',1,'BaseWidget']]],
  ['r_5fresult_5f1_5f_364',['r_result_1_',['../class_discrete_expectation.html#a343e13ceb1ec36e465dca922f6a95027',1,'DiscreteExpectation']]],
  ['r_5fresult_5f2_5f_365',['r_result_2_',['../class_discrete_expectation.html#a979a3fdf953598b639fe697b4241677a',1,'DiscreteExpectation']]],
  ['r_5fresult_5f3_5f_366',['r_result_3_',['../class_discrete_expectation.html#ab468661ca430dfeda7f967f8cfcb07ed',1,'DiscreteExpectation']]],
  ['repeat_5f_367',['repeat_',['../class_combinations_widget.html#a4fb689e14e6a2af43549e7062ba06143',1,'CombinationsWidget::repeat_()'],['../class_placements_widget.html#aa8c6f697e220e38317ab0c8e950818e0',1,'PlacementsWidget::repeat_()']]],
  ['result_5fline_5f_368',['result_line_',['../class_bernoulli_widget.html#aaf4055ccc57adad3cc2c718a302bea3d',1,'BernoulliWidget::result_line_()'],['../class_combinations_widget.html#a546a75a37a7a765412166bb85e300a1b',1,'CombinationsWidget::result_line_()'],['../class_integral_laplace_theorem.html#a6a002edd3c6b622ebf6d6080927c59e9',1,'IntegralLaplaceTheorem::result_line_()'],['../class_local_laplace_theorem.html#ab0ee4ea2566f2b8e85fed96fd9380aff',1,'LocalLaplaceTheorem::result_line_()'],['../class_placements_widget.html#a1f9d54714ac344995a57fac31a3b1b4e',1,'PlacementsWidget::result_line_()'],['../class_poisson_theorem.html#aff85b3bae08494f3e013514f613647f8',1,'PoissonTheorem::result_line_()']]],
  ['role_5f_369',['role_',['../class_base_widget.html#a922f57274f9a79fc7b71acc4d4c7574e',1,'BaseWidget']]]
];
