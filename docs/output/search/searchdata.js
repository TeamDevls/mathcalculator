var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstuvwx~",
  1: "abcdfhilmprstw",
  2: "a",
  3: "abcdfhilmpstw",
  4: "abcdefghilmnprstw~",
  5: "abcdefklmnprstvx",
  6: "ers",
  7: "hnopsu",
  8: "ehs",
  9: "ep"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "defines"
};

var indexSectionLabels =
{
  0: "Указатель",
  1: "Классы",
  2: "Пространства имен",
  3: "Файлы",
  4: "Функции",
  5: "Переменные",
  6: "Перечисления",
  7: "Элементы перечислений",
  8: "Свойства",
  9: "Макросы"
};

