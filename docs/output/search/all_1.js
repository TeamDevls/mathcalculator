var searchData=
[
  ['background_5fcolors_20',['background_colors',['../class_app_style.html#acfc11770c72522b8a009d91773573c77',1,'AppStyle']]],
  ['backgroundcolor_21',['backgroundColor',['../class_app_style.html#aaae4c88e68db731686a4447e62f827ee',1,'AppStyle']]],
  ['basewidget_22',['BaseWidget',['../class_base_widget.html',1,'BaseWidget'],['../class_base_widget.html#a91754f7f90922093a34e4a60d601bf99',1,'BaseWidget::BaseWidget()']]],
  ['basewidget_2ecpp_23',['basewidget.cpp',['../basewidget_8cpp.html',1,'']]],
  ['basewidget_2eh_24',['basewidget.h',['../basewidget_8h.html',1,'']]],
  ['bernoulliwidget_25',['BernoulliWidget',['../class_bernoulli_widget.html',1,'BernoulliWidget'],['../class_bernoulli_widget.html#ae68b048ac8e0337dfb5a27e162dcdf57',1,'BernoulliWidget::BernoulliWidget()']]],
  ['bernoulliwidget_2ecpp_26',['bernoulliwidget.cpp',['../bernoulliwidget_8cpp.html',1,'']]],
  ['bernoulliwidget_2eh_27',['bernoulliwidget.h',['../bernoulliwidget_8h.html',1,'']]],
  ['biggest_5fcorner_5fradius_5f_28',['biggest_corner_radius_',['../class_app_style.html#aac46293c9d177cc0a8888894d153d81e',1,'AppStyle']]],
  ['biggestcornerradius_29',['biggestCornerRadius',['../class_app_style.html#ab9cb3bb108f7a9f32d037ad86ea924ef',1,'AppStyle']]]
];
