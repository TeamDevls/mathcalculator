#include <gtest/gtest.h>
#include <algorithms.h>
#include <QString>
using namespace Algorithms::Internal;

TEST(LaplaceLocal_test, case_name) {
    EXPECT_NEAR(computeLaplaceLocal(100, 0.8, 70).value, 0.0043821397441143, 0.0000001);
    EXPECT_NEAR(computeLaplaceLocal(1000, 0.001, 990).value, 0.0, 0.0000001);

}
