#include <gtest/gtest.h>
#include <algorithms.h>
#include <QString>
using namespace Algorithms::Internal;

TEST(LaplaceIntegral_test, case_name) {
    EXPECT_NEAR(computeLaplaceIntegral(100, 0.05, 10, 30).value, 0.01069, 0.0003);
    EXPECT_NEAR(computeLaplaceIntegral(200, 0.5, 1, 199).value, 0.99998, 0.0001);
    EXPECT_NEAR(computeLaplaceIntegral(100, 0.05, 10, 30).value, 0.01069, 0.0003);
    EXPECT_NEAR(computeLaplaceIntegral(100, 0.01, 0, 100).value, 0.84379, 0.002);
    EXPECT_EQ(computeLaplaceIntegral(100, 0.8, 15, 15).value, 0.0);
}
