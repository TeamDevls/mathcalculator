#include <gtest/gtest.h>
#include <algorithms.h>
#include <QString>
using namespace Algorithms::Internal;

TEST(Bernoulli_test, case_name) {
    EXPECT_NEAR(computeBernoulli(70, 12, 0.3).value, 0.005861892661355684, 0.00005);
    EXPECT_NEAR(computeBernoulli(160, 90, 0.6).value, 0.03989581510285877, 0.00005);
    EXPECT_EQ(computeBernoulli(160, 160, 1.0).value, 1.0);
    EXPECT_EQ(computeBernoulli(160, 159, 1.0).value, 0.0);
    EXPECT_EQ(computeBernoulli(30, 0, 0.0).value, 1.0);
    EXPECT_EQ(computeBernoulli(30, 1, 0.0).value, 0.0);
}
