#include <gtest/gtest.h>
#include <algorithms.h>
#include <QString>
using namespace Algorithms::Internal;

TEST(Permutations_test, case_name) {
    EXPECT_EQ(computePermutations(0).value, 1.0);
    EXPECT_EQ(computePermutations(1).value, 1.0);
    EXPECT_EQ(computePermutations(2).value, 2.0);
    EXPECT_EQ(computePermutations(3).value, 6.0);
    EXPECT_EQ(computePermutations(10).value, 3628800.0);
    EXPECT_EQ(computePermutations(-2).err, Err::NegativeNumber);
}
