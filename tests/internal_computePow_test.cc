#include <gtest/gtest.h>
#include <algorithms.h>
#include <QString>
using namespace Algorithms::Internal;

TEST(Pow_test, case_name) {
    EXPECT_EQ(computePow(2.0, 2.0).value, 4.0);
    EXPECT_EQ(computePow(2.0, 10.0).value, 1024.0);
    EXPECT_EQ(computePow(1.0, 50.0).value, 1.0);
    EXPECT_EQ(computePow(-2.0, 2.0).value, 4.0);
    EXPECT_EQ(computePow(999999999.0, 9999999.0).err, Err::Overflow);
}
