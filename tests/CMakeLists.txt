set(CMAKE_AUTORCC ON)

find_package(Threads REQUIRED)
find_package(Qt6 COMPONENTS Core REQUIRED)
file(GLOB_RECURSE HEADERS *.h)
file(GLOB_RECURSE SOURCES *.cpp)
file(GLOB_RECURSE QRC ${PROJECT_SOURCE_DIR}/src/*.qrc)
file(GLOB_RECURSE ALGORITHMS_TESTS *.cc)
set(SOURCES ${SOURCES} ${PROJECT_SOURCE_DIR}/src/algorithms.cpp)
set(HEADERS ${HEADERS} ${PROJECT_SOURCE_DIR}/src/algorithms.h)

add_executable(${CMAKE_PROJECT_NAME}_unit_test ${SOURCES} ${HEADERS} ${QRC} ${ALGORITHMS_TESTS})
target_link_libraries(${CMAKE_PROJECT_NAME}_unit_test PRIVATE Threads::Threads Qt6::Core gtest_main)

include(GoogleTest)
add_test(
        NAME ${CMAKE_PROJECT_NAME}_unit_test
        COMMAND ${PROJECT_SOURCE_DIR}/tests/${CMAKE_PROJECT_NAME}_unit_test
)

