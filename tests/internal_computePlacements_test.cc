#include <gtest/gtest.h>
#include <algorithms.h>
#include <QString>
using namespace Algorithms::Internal;

TEST(Placements_test, case_name) {
    EXPECT_EQ(computePlacements(3, 15).err, Err::nLessThanK);
    EXPECT_EQ(computePlacements(15, 3).value, 2730.0);
    EXPECT_EQ(computePlacements(30, 0).value, 1.0);
    EXPECT_EQ(computePlacements(1000, 1).value, 1000.0);
    EXPECT_EQ(computePlacements(10, 7).value, 604800.0);
}
