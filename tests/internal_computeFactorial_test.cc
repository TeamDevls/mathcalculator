#include <gtest/gtest.h>
#include <algorithms.h>
#include <QString>
using namespace Algorithms::Internal;

TEST(Factorial_test, Positive_cases) {
    EXPECT_EQ(computeFactorial(0).value, 1.0);
    EXPECT_EQ(computeFactorial(1).value, 1.0);
    EXPECT_EQ(computeFactorial(2).value, 2.0);
    EXPECT_EQ(computeFactorial(3).value, 6.0);
    EXPECT_EQ(computeFactorial(10).value, 3628800.0);
    EXPECT_EQ(computeFactorial(-2).err, Err::NegativeNumber);
}
