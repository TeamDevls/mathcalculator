#include <gtest/gtest.h>
#include <algorithms.h>
#include <QString>
using namespace Algorithms::Internal;

TEST(Combinations_test, case_name) {
    EXPECT_EQ(computeCombinations(50, 6).value, 15890700.0);
    EXPECT_EQ(computeCombinations(100, 30).value, 29372339821610944823963760.0);
    EXPECT_EQ(computeCombinations(50, 6, 1).value, 28989675.0);
    EXPECT_EQ(computeCombinations(70, 2, 1).value, 2485.0);
}
