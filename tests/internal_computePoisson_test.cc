#include <gtest/gtest.h>
#include <algorithms.h>
#include <QString>
using namespace Algorithms::Internal;

TEST(Poisson_test, case_name) {
    EXPECT_NEAR(computePoisson(100, 5, 0.01).value, 0.00307, 0.0001);
    EXPECT_NEAR(computePoisson(500, 50, 0.05).value, 0, 0);
    EXPECT_NEAR(computePoisson(50, 5, 0.01).value, 0.000158, 0.0001);
    EXPECT_NEAR(computePoisson(1000, 5, 0.0005).value, 0.000158, 0.0001);
    EXPECT_EQ(computePoisson(1001, 8, 0.01).err, Err::PoissonLambdaTooBig);
}
